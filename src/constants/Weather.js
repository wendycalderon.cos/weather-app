export const SUNNY = "sunny";   
export const FOG = "fog";   
export const RAINDROP = "raindrop";   
export const SLEET = "sleet";   
export const HAZE = "haze";    
export const THUNDER = "thunder";    
export const DRIZZLE = "drizzle";    