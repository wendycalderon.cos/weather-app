import React from "react";
import WeatherExtraInfo from "./WeatherExtraInfo";
import WeatherTemperature from "./WeatherTemperature";
import PropTypes from "prop-types";


import "./styles.css";

const WeatherData = ({
  data: { temperature, weathState, humidity, wind }
}) => {

  return (
    <div className="weatherDataCont" >

      <WeatherTemperature
        temperature={temperature}
        weathState={weathState}
      ></WeatherTemperature>
      <WeatherExtraInfo humidity={humidity} wind={wind}></WeatherExtraInfo>
    </div>
  );
};

WeatherExtraInfo.propTypes={
     data: PropTypes.shape({
         temperature:PropTypes.number.isRequired,
         weathState:PropTypes.string.isRequired,
         humidity:PropTypes.number.isRequired,
         wind:PropTypes.string.isRequired,
     }),
};

export default WeatherData;
