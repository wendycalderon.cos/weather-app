import React from "react";
import WeatherIcons from "react-weathericons";
import PropTypes from "prop-types";
import { SUNNY, FOG, RAINDROP, SLEET, HAZE, DRIZZLE , THUNDER} from "../../../constants/Weather";
import './styles.css';
 
const icons = {
  [SUNNY]: "day-sunny",
  [FOG]: "day-fog",
  [RAINDROP]: "raindrop",
  [SLEET]: "day-sleet",
  [HAZE]: "day-haze",
  [THUNDER]: "day-thunderstore",
  [DRIZZLE]: "day-showers",
  [HAZE]: "day-haze",
};

const getWeatherIcon = weathState => {
  const icon = icons[weathState];

  const sizeIcon="4px";

  if (icon) return <WeatherIcons className="wicon" name={icon} size="2x" />;
  else return <WeatherIcons className="wicon" name={"day-sunny"} size="2x" />;
};

const WeatherTemperature = ({ temperature, weathState }) => (
  <div className="weatherTemperatureCont">
    {getWeatherIcon(weathState)}
    <span className="temperature">{`${temperature}`}</span>
    <span className="temperatureType">{`C`}</span>
  </div>
);

//Validaciones por parte de la herramienta PropTypes
WeatherTemperature.propTypes={
    temperature: PropTypes.number.isRequired,
    weathState: PropTypes.string.isRequired,
};

export default WeatherTemperature;
