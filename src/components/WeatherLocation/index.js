import React, { Component } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import Location from "./Location";
import WeatherData from "./WeatherData";
import PropTypes from "prop-types";
import getUrlWeatherByCity from "../../services/getUrlWeatherByCyty";
import "./styles.css";
import {
  THUNDER,
  DRIZZLE,
  SLEET,
  HAZE,
  RAINDROP,
  FOG
} from "../../constants/Weather";

class WeatherLocation extends Component {
  constructor(props) {
    super(props);
    const { city } = props;
    this.state = {
      city,
      data: null
    };
  }

  getWeatherState = weather => {

    const { id } = weather;

    if (id < 300) {
      return THUNDER;
    } else if (id < 400) {
      return RAINDROP;
    } else if (id < 600) {
      return DRIZZLE;
    } else if (id < 700) {
      return HAZE;
    }else if (id > 800) {
      return FOG;
    }else{
       return SLEET;
    }
  };


  getData = weather_data => {

    const { humidity, temp } = weather_data.main;
    const { speed } = weather_data.wind;
    const  weathState  = this.getWeatherState(weather_data.weather[0]);
    

    const data = {
      humidity,
      temperature: temp,
      weathState,
      wind: `${speed} m/s`
    };

    return data;
  };

  componentDidMount() {
    
    this.handleUpdateClick();
  }

  handleUpdateClick = () => {
    const api_weather = getUrlWeatherByCity(this.state.city);

    fetch(api_weather)
      .then(resolve => {
        return resolve.json();
      })
      .then(data => {
        const newWeather = this.getData(data);

        this.setState({
          data: newWeather
        });
      });
  };

  render() {
    const {onWeatherLocationClick}=this.props;

    const { city, data } = this.state;

    return (
      <div className="weatherLocationCont" onClick={onWeatherLocationClick}>
        <Location city={city}></Location>
        {data ? (
          <WeatherData data={data}></WeatherData>
        ) : (
          <CircularProgress size={60}></CircularProgress>
        )}
      </div>
    );
  }
}

WeatherLocation.propTypes = {
  city: PropTypes.string.isRequired,
  onWeatherLocationClick: PropTypes.func,
};

export default WeatherLocation;
