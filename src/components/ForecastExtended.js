import React, { Component } from "react";
import PropTypes from "prop-types";
import "./WeatherLocation/styles.css";
import ForecastItem from "./ForecastItem";

const days = ["lunes", "martes", "miercoles"];

const data={
    temperature:10, 
    humidity:10,
    weathState:'Nombre',
    wind: 'nombre'
}
class ForecastExtended extends Component {
  renderForecastItemDays() {
    return days.map((day, index) => <ForecastItem weekDay={day} key={index} hour={20} data={data}></ForecastItem>);
  }

  render() {
    const { city } = this.props;

    return (
      <div>
        <h1>Pronóstico extendido para {city}</h1>
        {this.renderForecastItemDays()}
      </div>
    );
  }
}
ForecastExtended.propTypes = {
  city: PropTypes.string.isRequired
};

export default ForecastExtended;
